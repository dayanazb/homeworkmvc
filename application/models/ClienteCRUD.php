<?php


class ClienteCRUDModel extends CI_Model{


    public function get_clienteCRUD(){
        if(!empty($this->input->get("search"))){
          $this->db->like('cedula', $this->input->get("search"));
          $this->db->or_like('nombre', $this->input->get("search")); 
          $this->db->or_like('apellido', $this->input->get("search"));
          $this->db->or_like('telefono', $this->input->get("search"));
        }
        $query = $this->db->get("clientes");
        return $query->result();
    }


    public function insert_cliente()
    {    
        $data = array(
            'cedula' => $this->input->post('cedula'),
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'telefono' => $this->input->post('telefono')
        );
        return $this->db->insert('clientes', $data);
    }


    public function update_cliente($id) 
    {
        $data = array(
            'cedula' => $this->input->post('cedula'),
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'telefono' => $this->input->post('telefono')
        );
        if($id==0){
            return $this->db->insert('clientes',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('clientes',$data);
        }        
    }


    public function find_cliente($id)
    {
        return $this->db->get_where('clientes', array('id' => $id))->row();
    }


    public function delete_cliente($id)
    {
        return $this->db->delete('clientes', array('id' => $id));
    }
}
?>