<?php


class ClienteCRUDModel extends CI_Model{


    public function get_cliente(){
        if(!empty($this->input->get("search"))){
          $this->db->like('cedula', $this->input->get("search"));
          $this->db->or_like('nombre', $this->input->get("search")); 
          $this->db->or_like('apellido', $this->input->get("search"));
          $this->db->or_like('telefono', $this->input->get("search"));
        }
        $query = $this->db->get("cliente");
        return $query->result();
    }


    public function insert_cliente()
    {    
        $data = array(
            'cedula' => $this->input->post('cedula'),
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'telefono' => $this->input->post('telefono')
        );
        return $this->db->insert('cliente', $data);
    }


    public function update_cliente($id) 
    {
        $data = array(
            'cedula' => $this->input->post('cedula'),
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'telefono' => $this->input->post('telefono')
        );
        if($id==0){
            return $this->db->insert('cliente',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('cliente',$data);
        }        
    }


    public function find_cliente($id)
    {
        return $this->db->get_where('cliente', array('id' => $id))->row();
    }


    public function delete_cliente($id)
    {
        return $this->db->delete('cliente', array('id' => $id));
    }
}
?>