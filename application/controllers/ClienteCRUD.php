<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ClienteCRUD extends CI_Controller {

	public $clienteCRUD;

	/**
	    * Get All Data from this method.
	    *
	    * @return Response
	   */
	public function __construct() {
	      parent::__construct(); 


	      $this->load->library('form_validation');
	      $this->load->library('session');
	      $this->load->model('ClienteCRUDModel');


	      $this->clienteCRUD = new ClienteCRUDModel;
	   }
	   /**

    * Display Data this method.
    *
    * @return Response
   */
   public function index()
   {
       $data['data'] = $this->clienteCRUD->get_clienteCRUD();


       $this->load->view('theme/header');       
       $this->load->view('clienteCRUD/list',$data);
       $this->load->view('theme/footer');
   }
    /**
    * Show Details this method.
    *
    * @return Response
   */
   public function show($id)
   {
      $cliente = $this->clienteCRUD->find_cliente($id);


      $this->load->view('theme/header');
      $this->load->view('clienteCRUD/show',array('cliente'=>$cliente));
      $this->load->view('theme/footer');
   }


   /**
    * Create from display on this method.
    *
    * @return Response
   */
   public function create()
   {
      $this->load->view('theme/header');
      $this->load->view('clienteCRUD/create');
      $this->load->view('theme/footer');   
   }


   /**
    * Store Data from this method.
    *s
    * @return Response
   */
   public function store()
   {
        $this->form_validation->set_rules('cedula', 'Cedula', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('apellido', 'Apellido', 'required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'required');


        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('clienteCRUD/create'));
        }else{
           $this->clienteCRUD->insert_cliente();
           redirect(base_url('clienteCRUD'));
        }
    }


   /**
    * Edit Data from this method.
    *
    * @return Response
   */
   public function edit($id)
   {
       $cliente = $this->clienteCRUD->find_cliente($id);


       $this->load->view('theme/header');
       $this->load->view('clienteCRUD/edit',array('cliente'=>$cliente));
       $this->load->view('theme/footer');
   }


   /**
    * Update Data from this method.
    *
    * @return Response
   */
   public function update($id)
   {
        $this->form_validation->set_rules('cedula', 'Cedula', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('apellido', 'Apellido', 'required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'required');


        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('clienteCRUD/edit/'.$id));
        }else{ 
          $this->clienteCRUD->update_cliente($id);
          redirect(base_url('clienteCRUD'));
        }
   }


   /**
    * Delete Data from this method.
    *
    * @return Response
   */
   public function delete($id)
   {
       $cliente = $this->clienteCRUD->delete_cliente($id);
       redirect(base_url('clienteCRUD'));
   }
}

