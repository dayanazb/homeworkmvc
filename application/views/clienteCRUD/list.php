
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Codeigniter 3 CRUD Example from scratch</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="<?php echo base_url('clienteCRUD/create') ?>"> Crear un nuevo Cliente</a>
        </div>
    </div>
</div>


<table class="table table-bordered">


  <thead>
      <tr>
          <th>Cédula</th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Telefono</th>
          <th width="220px">Action</th>
      </tr>
  </thead>


  <tbody>
   <?php foreach ($data as $cliente) { ?>      
      <tr>
          <td><?php echo $cliente->cedula; ?></td>
          <td><?php echo $cliente->nombre; ?></td>     
          <td><?php echo $cliente->apellido; ?></td>
          <td><?php echo $cliente->telefono; ?></td>      
      <td>
        <form method="DELETE" action="<?php echo base_url('clienteCRUD/delete/'.$cliente->id);?>">
          <a class="btn btn-info" href="<?php echo base_url('clienteCRUD/'.$cliente->id) ?>"> show</a>

 
         <a class="btn btn-primary" href="<?php echo base_url('clienteCRUD/edit/'.$cliente->id) ?>"> Edit</a>
          <button type="submit" class="btn btn-danger"> Delete</button>
        </form>
      </td>     
      </tr>
      <?php } ?>
  </tbody>


</table>